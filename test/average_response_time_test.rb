require 'test_helper'


describe "AverageResponseTime" do

  let(:response_timer_class) {
    UrlResponseTimer::AverageResponseTime.new
  }

  describe ".get_average_response_time" do

    it "calculates average from successful responses" do
      response_timer_class.stubs(:get_response_time).returns(
        0.023,
        0.044,
        nil,
        0.032,
        nil,
      )

      response_timer_class.expects(:result_callback).with(3, 0.033).once
      a = response_timer_class.get_average_response_time("dummy_url", 0.1, 0.4, nil)
      assert_equal 0.033, a
    end

    it "return nil on no successful response" do
      response_timer_class.stubs(:get_response_time).returns(nil)

      response_timer_class.expects(:result_callback).with(0, nil).once
      a = response_timer_class.get_average_response_time("dummy_url", 0.1, 0.2, nil)
      assert_nil a
    end

    describe "with requests that take longer than interval" do

      module UrlResponseTimer
        class AverageResponseTime

          def get_response_time(url, max_depth)
            sleep 0.2
            return 0.2
          end
        end
      end

      it "calculate with overlapping requests if responded during test interval" do
        response_timer_class.expects(:result_callback).with(2, 0.2).once
        response_timer_class.get_average_response_time("dummy_url", 0.05, 0.3, nil)
      end
    end

  end
end
