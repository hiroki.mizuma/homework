require 'test_helper'


describe "QueryMethods::NetHttp" do

  let(:query_method) {
    UrlResponseTimer::QueryMethods::NetHttp.new
  }

  describe ".get_response_time" do

    describe "with failed response" do

      it "returns nil" do
        query_method.stubs(:get_response_time).returns(nil)
        assert_nil query_method.get_response_time("dummy_url")
      end
    end

    describe "with successful response" do

      module UrlResponseTimer
        module QueryMethods
          class NetHttp

            private

            def request_url(url, max_depth)
              sleep 0.2
              return true
            end
          end
        end
      end

      it "returns response time" do
        response_time = query_method.get_response_time("dummy_url")
        assert_equal response_time.round(1), 0.2
      end
    end
  end

  describe ".setup_net_http" do

    it "parses valid url" do
      result = query_method.send(:setup_net_http, "http://test.local/uri/path/")

      assert_equal "/uri/path/", result[:path]
      assert_equal true, result[:http].is_a?(Net::HTTP)
      assert_equal false, result[:http].use_ssl?
    end

    it "parses https url" do
      result = query_method.send(:setup_net_http, "https://test.local")

      assert_equal "/", result[:path]
      assert_equal true, result[:http].is_a?(Net::HTTP)
      assert_equal true, result[:http].use_ssl?
    end

    it "reuses existing key" do
      dummy = {
        "https://test.local" => {
          http: 'dummy',
          path: 'dummy'
        }
      }

      query_method.instance_variable_set(:@http, dummy)
      query_method.send(:setup_net_http, "https://test.local") == dummy
    end
  end

  describe ".response_handler" do

    let(:successful_response) {
      Net::HTTPSuccess.new(2, '200', 'msg')
    }

    let(:redirect_response) {
       Net::HTTPRedirection.new(2, '302', 'msg')
    }

    it "return true with success response" do
      assert_equal true, query_method.send(:response_handler, successful_response, 2)
    end

    it "makes recursive call with redirect response" do
      redirect_response.stubs(:[]).with("location").returns(
        "http://redir.test",
      )

      query_method.expects(:request_url).with("http://redir.test", 1).once
      query_method.send(:response_handler, redirect_response, 2)
    end

  end
end
