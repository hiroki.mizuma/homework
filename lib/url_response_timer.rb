require "url_response_timer/version"
require "url_response_timer/logger"
require "url_response_timer/errors"
require "url_response_timer/average_response_time"

module UrlResponseTimer
  class << self
    attr_accessor :logger
  end
end

# placeholder default logger that just calls puts
UrlResponseTimer.logger = UrlResponseTimer::Logger.new
