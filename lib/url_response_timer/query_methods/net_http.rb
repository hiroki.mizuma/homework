require 'uri'
require 'net/http'
require 'openssl'

module UrlResponseTimer
  module QueryMethods
    class NetHttp

      def initialize
        @http = {}
      end

      #
      # Time response to get URL
      #
      # url: URL to test. Should be in format like http://domain.tld/path
      # max_depth: Number of redirects to follow. Disabled if nil.
      #
      # returns time taken on success, nil on failure
      #
      def get_response_time(url, max_depth=nil)
        # preload data for default URL
        # setup_net_http(url)

        start_time = Time.now

        if request_url(url, max_depth)
          return Time.now - start_time
        else

          return nil
        end

      rescue => e
        UrlResponseTimer.logger.error("** #{self.class.name} #{__method__} #{e.message}")
        return nil
      end

      private

      # make request and recursively call if redirect is enabled
      # return true on success, false on failure
      def request_url(url, max_depth=nil)
        http = setup_net_http(url)
        http[:http].request_get(http[:path]) do |response|

          return response_handler(response, max_depth)
        end
      end

      # Setup Net::HTTP for URL.
      # Save instance in @http to reuse. URL should be all same other than redirects.
      def setup_net_http(url)
        if @http.has_key?(url)
          return @http[url]

        else
          uri = URI.parse(url)

          if !uri.port.nil? &&
            !uri.host.nil? &&
            !uri.scheme.nil? &&
            !uri.path.nil?

            http = Net::HTTP.new(uri.host, uri.port)

            case uri.scheme
            when 'https'
              http.use_ssl = true
              http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            end
          end

          @http[url] = {
            http: http,
            path: uri.request_uri
          }
        end
      end

      # parse the net http response
      # return true on success, false on failure
      def response_handler(response, max_depth)
        case response
        when Net::HTTPRedirection
          # Ignore redirect response and return for response time check if max_depth is a non integer
          if !max_depth.is_a?(Integer)
            return true

          # Otherwise follow redirect until success response or max_depth is reached
          elsif max_depth > 0
            UrlResponseTimer.logger.info("** following redirect #{response['location']}")
            return request_url(response['location'], max_depth - 1)

          # At max redirect - return for current time
          else
            UrlResponseTimer.logger.info("** reached max redirect depth")
            return true
          end

        when Net::HTTPSuccess
          return true

        else
          UrlResponseTimer.logger.error("** received http failure response")
          return false
        end
      end
    end

  end
end
