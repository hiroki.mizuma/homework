require 'thread'
require 'url_response_timer/query_methods/net_http'

module UrlResponseTimer
  class AverageResponseTime

    def initialize
      @http = {}
      @mutex = Mutex.new
      @query_method = UrlResponseTimer::QueryMethods::NetHttp.new
    end

    #
    # Print average response time per interval to get URL during a test window.
    # Responses that take long and not received in the test window are dropped.
    #
    # url: URL to test. Should be in format like http://domain.tld/path
    # test_length: Check responses over test_length seconds.
    # test_interval: Send request every test_interval seconds during test window.
    # max_depth: Follow redirect up to max_depth paths. Disabled if nil.
    #
    # returns averge response time in seconds on success, nil on failure.
    #
    def get_average_response_time(url, test_interval, test_length, max_depth=nil)
      total_response_time = 0
      response_count = 0
      average_response_time = nil

      running = true
      threads = []

      timer = Thread.new do
        sleep test_length
        running = false

        threads.each do |t|
          # don't wait for incomplete requests. exit and don't add result to calculation
          if t.alive?
            t.exit
          end
        end
      end

      while running do
        l = threads.length

        threads << Thread.new do
          index = l
          UrlResponseTimer.logger.info("request #{index} #{url}")

          t = get_response_time(url, max_depth)

          if !t.nil?
            UrlResponseTimer.logger.info("response #{index} in #{t*1000} ms")

            @mutex.synchronize do
              total_response_time += t
              response_count += 1
            end

          else
            UrlResponseTimer.logger.error("request #{index} failed")
          end
        end

        sleep test_interval
      end

      timer.join

      if response_count > 0
        average_response_time = total_response_time/response_count

        UrlResponseTimer.logger.info("response count: #{response_count}")
        UrlResponseTimer.logger.info("average response time: #{average_response_time*1000} ms")
      else
        UrlResponseTimer.logger.error("no responses received during run")
      end

      result_callback(response_count, average_response_time)

      return average_response_time
    end

    private

    def get_response_time(url, max_depth)
      @query_method.get_response_time(url, max_depth)
    end

    def result_callback(response_count, average_response_time)
    end

  end
end
