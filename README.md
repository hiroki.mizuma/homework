# UrlResponseTimer

Measure average response time to URL over time. Homework sample project.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'url_response_timer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install url_response_timer

## Usage

Initialize:

    require 'url_response_timer'
    
    t = UrlResponseTimer::AverageResponseTime.new
    
Run test to https://gitlab.com over 60 seconds in 10 second intervals.
    
    t.get_average_response_time("https://gitlab.com", 10, 60)
    
Optionally follow n redirects by setting.

    t.get_average_response_time("https://gitlab.com", 10, 60, n)
    

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
